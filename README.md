# iterm2-genesis-syntax

An [iTerm2](https://www.iterm2.com/) color scheme designed around the Atom [genesis-syntax](https://atom.io/themes/genesis-syntax) theme.

## Screenshot

![iterm2-genesis-syntax](screenshot.png)


## License

[MIT License](./LICENSE) © David Skuza
